package edu.demo.rest.service;

import edu.demo.rest.repository.PostRepository;
import edu.demo.rest.entity.Post;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class PostService {

    private final PostRepository postRepository;

    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public List<Post> all() {
        return postRepository.findAll();
    }

    public Optional<Post> byId(UUID id) {
        return postRepository.findById(id);
    }

    public void delete(Post post) {
        postRepository.delete(post);
    }

    public Optional<Post> createPost(){
        Post post = Post.builder()
                .content("Hellow!!!!".concat(LocalDateTime.now().toString()))
                .id(UUID.randomUUID())
                .build();

        log.info("Post: {}", post);

        Post savedPost = postRepository.save(post);

        return Optional.ofNullable(savedPost);
    }

}
