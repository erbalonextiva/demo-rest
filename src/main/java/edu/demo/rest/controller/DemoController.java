package edu.demo.rest.controller;

import edu.demo.rest.entity.Post;
import edu.demo.rest.exception.EntityNotFoundException;
import edu.demo.rest.service.PostService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/posts")
public class DemoController {

    private final PostService postService;

    public DemoController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Post> allPosts() {
        return postService.all();
    }

    @GetMapping("/{uuid}")
    @ResponseStatus(HttpStatus.OK)
    public Post getPost(@PathVariable UUID uuid) {
        return postService.byId(uuid).orElseThrow(EntityNotFoundException::new);
    }

    @DeleteMapping("/{uuid}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deletePost(@PathVariable UUID uuid) {
        Post post = postService.byId(uuid).orElseThrow(EntityNotFoundException::new);
        postService.delete(post);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void createPost() {
        postService.createPost().orElseThrow(RuntimeException::new);
    }

}
