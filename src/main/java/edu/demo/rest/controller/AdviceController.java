package edu.demo.rest.controller;

import edu.demo.rest.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestControllerAdvice
@Slf4j
public class AdviceController {

    @ExceptionHandler(value = {EntityNotFoundException.class})
    void handleNotFoundException(EntityNotFoundException e, HttpServletResponse response) throws IOException {
        log.error("Entity not found!, more details: ", e);
        response.sendError(HttpStatus.NOT_FOUND.value(), "Resource not found.");
    }


}
